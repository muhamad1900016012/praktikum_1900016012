import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {  Routes,RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import {materialdesain} from '../material/material';
import { ImageComponent } from './image/image.component';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FileUploaderComponent } from './file-uploader/file-uploader.component';
const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:'dashboard',
        component:DashboardComponent
      },
      {
        path:'image',
        component:ImageComponent
      },
      {
        path:'product',
        component:ProductComponent
      }
    ]
  }

]

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ImageComponent,
    ProductComponent,
    ProductDetailComponent,
    FileUploaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    materialdesain,

  ]
})
export class AdminModule { }
